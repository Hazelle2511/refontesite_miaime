# refontesite_miaime

# Contexte du projet

 # Le propriétaire du site www.mi-aime-a-ou aimerait donner à son site une nouvelle jeunesse, faites une refonte graphique du site web pour le client.

# Critères de performance

 # Le résultat final doit être fonctionnel, avec la possibilités de naviguer entre les pages. Le design doit aussi être cohérent avec le thème global du site d'origine "tourisme à la réunion"

# Modalités d'évaluation

 # Présentation de projet sous forme d'oral
